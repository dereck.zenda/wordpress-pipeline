# Blog App

[Blog App](https://blogapp.dereckzenda.io) is a blog site build using open source tool [WordPress](https://wordpress.org) based on PHP and MySQL. This is designed to be run on kubernetes. The app consists of there components with the docker images build separately but are dependent.

### App Components
The app consists of the following components
 - App - this contains the base application and code
 - mysql - database for the app
 - redis - caching for the app

### Defined Environment Variables 
The following environment variables are defined for the app and database components

###### APP
-	`WORDPRESS_DB_HOST=...` (defaults to the IP and port of the linked `mysql` container)
-	`WORDPRESS_DB_USER=...` (defaults to "root")
-	`WORDPRESS_DB_PASSWORD=...` (defaults to the value of the `MYSQL_ROOT_PASSWORD` environment variable from the linked `mysql` container)
-	`WORDPRESS_DB_NAME=...` (defaults to "wordpress")
-	`WORDPRESS_TABLE_PREFIX=...` (defaults to "", only set this when you need to override the default table prefix in wp-config.php)

###### MYSQL
-	`MYSQL_ROOT_PWD`=...` (mysql root password)
-	`MYSQL_USER=...` (define a user to be used in mysql)
-	`MYSQL_USER_PWD=...` (password for the defined mysql user)

## Deployment
The deployment of the application is done through a CI/CD pipeline through a defined jenkins pipeline job aided  by scripts or gitlab ci/cd as defined in the `.gitlab.ci.yml`
K8 deployment manifests are defined in the templates folder and deployed in the following order

- mysql database `kubectl appy -f templates/mysql.yaml`
- redis for cache to be configured once the app is running `kubectl appy -f templates/redis.yaml`
- blogapp - the application deployment. `kubectl appy -f templates/blog-app.yaml`

The app is designed to scale up to a maximun of 10 pods as defined in the HPA based on cpu,memory utilization and number of requests metrics. Single instances of the database and the cache server are deployed but can also be scaled depending on the storage used. A pod disruption budget is also defined for each component, this ensures there is at least a pod running for that component in the event of a node/cluster failure.

Once the app is deployed, it should be accessible through https://blogapp.dereckzenda.io



